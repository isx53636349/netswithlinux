					ejercicio 1

-	Paramos el NetworkManager y el dhclient

[root@j19 isx53636349]# systemctl stop NetworkManager
[root@j19 isx53636349]# dhclient -r

-	Pongo las dos ip

[root@j19 isx53636349]# ip a a 10.7.19.100/24 dev enp3s0
[root@j19 isx53636349]# ip a a 10.8.8.119/16 dev enp3s0

-	hago ping

[root@j19 isx53636349]# ping 10.8.8.101
PING 10.8.8.101 (10.8.8.101) 56(84) bytes of data.
64 bytes from 10.8.8.101: icmp_seq=1 ttl=64 time=0.207 ms
64 bytes from 10.8.8.101: icmp_seq=2 ttl=64 time=0.175 ms
64 bytes from 10.8.8.101: icmp_seq=3 ttl=64 time=0.194 ms
64 bytes from 10.8.8.101: icmp_seq=4 ttl=64 time=0.183 ms
64 bytes from 10.8.8.101: icmp_seq=5 ttl=64 time=0.177 ms
64 bytes from 10.8.8.101: icmp_seq=6 ttl=64 time=0.194 ms
64 bytes from 10.8.8.101: icmp_seq=7 ttl=64 time=0.195 ms

[root@j19 isx53636349]# ping 10.7.19.1
PING 10.7.19.1 (10.7.19.1) 56(84) bytes of data.
64 bytes from 10.7.19.1: icmp_seq=1 ttl=64 time=0.186 ms
64 bytes from 10.7.19.1: icmp_seq=2 ttl=64 time=0.194 ms
64 bytes from 10.7.19.1: icmp_seq=3 ttl=64 time=0.211 ms
64 bytes from 10.7.19.1: icmp_seq=4 ttl=64 time=0.226 ms


					ejercicio 2

-	Añadimos una ip adicional

[root@j19 isx53636349]# ip a a 10.9.9.219/24 dev enp3s0

-	Añadimos una ruta estatica

[root@j19 isx53636349]# ip r a 10.6.6.0/24 via 10.9.9.1
[root@j19 isx53636349]# ping 10.6.6.2
PING 10.6.6.2 (10.6.6.2) 56(84) bytes of data.
64 bytes from 10.6.6.2: icmp_seq=1 ttl=63 time=0.614 ms
64 bytes from 10.6.6.2: icmp_seq=2 ttl=63 time=0.459 ms
64 bytes from 10.6.6.2: icmp_seq=3 ttl=63 time=0.567 ms
64 bytes from 10.6.6.2: icmp_seq=4 ttl=63 time=0.544 ms
^C
--- 10.6.6.2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3000ms
rtt min/avg/max/mdev = 0.459/0.546/0.614/0.056 ms



					ejercicio 3

- capturo paquetes ping 10.6.6.2

[root@j19 isx53636349]# tshark -i enp3s0 -w /tmp/ping19.pcap -a duration:5
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp3s0'
121 

-	cambio los permisos para que lo pueda leer

[root@j19 isx53636349]# chmod 666 /tmp/ping19.pcap

-	Muestra por pantalla el ttl usando

[root@j19 isx53636349]# tshark -i enp3s0 -c 2 -w /tmp/ping.pcap icmp
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp3s0'
2 

-	Observa el ttl con la instruccion:

[root@j19 isx53636349]# tshark -r /tmp/ping.pcap
Running as user "root" and group "root". This could be dangerous.
  1 0.000000000   10.9.9.219 → 10.6.6.2     ICMP 98 Echo (ping) request  id=0x0aa3, seq=389/34049, ttl=64
  2 0.000505997     10.6.6.2 → 10.9.9.219   ICMP 98 Echo (ping) reply    id=0x0aa3, seq=389/34049, ttl=63 (request in 1)


				ejercicio 4
				
-	conecta una interfaz usb cambiale el nombre usb19 cambiale mac a 44:44:44:00:00:19

[root@j19 isx53636349]# ip link set usb0 down
[root@j19 isx53636349]# ip link set usb0 name usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 10.7.19.100/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.8.8.119/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.219/24 scope global enp3s0
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff


[root@j19 isx53636349]# ip link set address 44:44:44:00:00:19 dev usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 10.7.19.100/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.8.8.119/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.219/24 scope global enp3s0
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
[root@j19 isx53636349]# ip link set usb19 up


-	pon ip a usb19

root@j19 isx53636349]# ip a a 10.5.5.119/24 dev usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 10.7.19.100/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.8.8.119/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.219/24 scope global enp3s0
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
    inet 10.5.5.119/24 scope global usb19
       valid_lft forever preferred_lft forever
    inet6 fe80::4644:44ff:fe00:19/64 scope link 
       valid_lft forever preferred_lft forever

root@j19 isx53636349]# ip a a 10.5.5.119/24 dev usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 10.7.19.100/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.8.8.119/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.219/24 scope global enp3s0
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
    inet 10.5.5.119/24 scope global usb19
       valid_lft forever preferred_lft forever
    inet6 fe80::4644:44ff:fe00:19/64 scope link 
       valid_lft forever preferred_lft forever

root@j19 isx53636349]# ip a a 10.5.5.119/24 dev usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 10.7.19.100/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.8.8.119/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.219/24 scope global enp3s0
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
    inet 10.5.5.119/24 scope global usb19
       valid_lft forever preferred_lft forever
    inet6 fe80::4644:44ff:fe00:19/64 scope link 
       valid_lft forever preferred_lft forever


-	hago ping a 10.5.5.1

[root@j19 isx53636349]# ping 10.5.5.1

PING 10.5.5.1 (10.5.5.1) 56(84) bytes of data.
64 bytes from 10.5.5.1: icmp_seq=1 ttl=64 time=0.655 ms
64 bytes from 10.5.5.1: icmp_seq=2 ttl=64 time=0.726 ms
64 bytes from 10.5.5.1: icmp_seq=3 ttl=64 time=0.689 ms
64 bytes from 10.5.5.1: icmp_seq=4 ttl=64 time=0.664 ms
64 bytes from 10.5.5.1: icmp_seq=5 ttl=64 time=0.626 ms
64 bytes from 10.5.5.1: icmp_seq=6 ttl=64 time=0.721 ms
64 bytes from 10.5.5.1: icmp_seq=7 ttl=64 time=0.691 ms
64 bytes from 10.5.5.1: icmp_seq=8 ttl=64 time=0.656 ms
64 bytes from 10.5.5.1: icmp_seq=9 ttl=64 time=0.629 ms
64 bytes from 10.5.5.1: icmp_seq=10 ttl=64 time=0.720 ms

				ejercicio 5
				
-	hacer flush de todas las ip

					ejercicio 1

-	Paramos el NetworkManager y el dhclient

[root@j19 isx53636349]# systemctl stop NetworkManager
[root@j19 isx53636349]# dhclient -r

-	Pongo las dos ip

[root@j19 isx53636349]# ip a a 10.7.19.100/24 dev enp3s0
[root@j19 isx53636349]# ip a a 10.8.8.119/16 dev enp3s0

-	hago ping

[root@j19 isx53636349]# ping 10.8.8.101
PING 10.8.8.101 (10.8.8.101) 56(84) bytes of data.
64 bytes from 10.8.8.101: icmp_seq=1 ttl=64 time=0.207 ms
64 bytes from 10.8.8.101: icmp_seq=2 ttl=64 time=0.175 ms
64 bytes from 10.8.8.101: icmp_seq=3 ttl=64 time=0.194 ms
64 bytes from 10.8.8.101: icmp_seq=4 ttl=64 time=0.183 ms
64 bytes from 10.8.8.101: icmp_seq=5 ttl=64 time=0.177 ms
64 bytes from 10.8.8.101: icmp_seq=6 ttl=64 time=0.194 ms
64 bytes from 10.8.8.101: icmp_seq=7 ttl=64 time=0.195 ms

[root@j19 isx53636349]# ping 10.7.19.1
PING 10.7.19.1 (10.7.19.1) 56(84) bytes of data.
64 bytes from 10.7.19.1: icmp_seq=1 ttl=64 time=0.186 ms
64 bytes from 10.7.19.1: icmp_seq=2 ttl=64 time=0.194 ms
64 bytes from 10.7.19.1: icmp_seq=3 ttl=64 time=0.211 ms
64 bytes from 10.7.19.1: icmp_seq=4 ttl=64 time=0.226 ms


					ejercicio 2

-	Añadimos una ip adicional

[root@j19 isx53636349]# ip a a 10.9.9.219/24 dev enp3s0

-	Añadimos una ruta estatica

[root@j19 isx53636349]# ip r a 10.6.6.0/24 via 10.9.9.1
[root@j19 isx53636349]# ping 10.6.6.2
PING 10.6.6.2 (10.6.6.2) 56(84) bytes of data.
64 bytes from 10.6.6.2: icmp_seq=1 ttl=63 time=0.614 ms
64 bytes from 10.6.6.2: icmp_seq=2 ttl=63 time=0.459 ms
64 bytes from 10.6.6.2: icmp_seq=3 ttl=63 time=0.567 ms
64 bytes from 10.6.6.2: icmp_seq=4 ttl=63 time=0.544 ms
^C
--- 10.6.6.2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3000ms
rtt min/avg/max/mdev = 0.459/0.546/0.614/0.056 ms



					ejercicio 3

- capturo paquetes ping 10.6.6.2

[root@j19 isx53636349]# tshark -i enp3s0 -w /tmp/ping19.pcap -a duration:5
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp3s0'
121 

-	cambio los permisos para que lo pueda leer

[root@j19 isx53636349]# chmod 666 /tmp/ping19.pcap

-	Muestra por pantalla el ttl usando

[root@j19 isx53636349]# tshark -i enp3s0 -c 2 -w /tmp/ping.pcap icmp
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp3s0'
2 

-	Observa el ttl con la instruccion:

[root@j19 isx53636349]# tshark -r /tmp/ping.pcap
Running as user "root" and group "root". This could be dangerous.
  1 0.000000000   10.9.9.219 → 10.6.6.2     ICMP 98 Echo (ping) request  id=0x0aa3, seq=389/34049, ttl=64
  2 0.000505997     10.6.6.2 → 10.9.9.219   ICMP 98 Echo (ping) reply    id=0x0aa3, seq=389/34049, ttl=63 (request in 1)


				ejercicio 4
				
-	conecta una interfaz usb cambiale el nombre usb19 cambiale mac a 44:44:44:00:00:19

[root@j19 isx53636349]# ip link set usb0 down
[root@j19 isx53636349]# ip link set usb0 name usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 10.7.19.100/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.8.8.119/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.219/24 scope global enp3s0
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff


[root@j19 isx53636349]# ip link set address 44:44:44:00:00:19 dev usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 10.7.19.100/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.8.8.119/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.219/24 scope global enp3s0
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
[root@j19 isx53636349]# ip link set usb19 up


-	pon ip a usb19

root@j19 isx53636349]# ip a a 10.5.5.119/24 dev usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 10.7.19.100/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.8.8.119/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.219/24 scope global enp3s0
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
    inet 10.5.5.119/24 scope global usb19
       valid_lft forever preferred_lft forever
    inet6 fe80::4644:44ff:fe00:19/64 scope link 
       valid_lft forever preferred_lft forever

root@j19 isx53636349]# ip a a 10.5.5.119/24 dev usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 10.7.19.100/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.8.8.119/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.219/24 scope global enp3s0
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
    inet 10.5.5.119/24 scope global usb19
       valid_lft forever preferred_lft forever
    inet6 fe80::4644:44ff:fe00:19/64 scope link 
       valid_lft forever preferred_lft forever

root@j19 isx53636349]# ip a a 10.5.5.119/24 dev usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 10.7.19.100/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.8.8.119/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 10.9.9.219/24 scope global enp3s0
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
    inet 10.5.5.119/24 scope global usb19
       valid_lft forever preferred_lft forever
    inet6 fe80::4644:44ff:fe00:19/64 scope link 
       valid_lft forever preferred_lft forever
       
       
			ejercicio 5
			

-	hago flush de todas las ip

[root@j19 isx53636349]# ip a f enp3s0
[root@j19 isx53636349]# ip a f usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
[root@j19 isx53636349]# 

-	conecto dhcp por eth0

[root@j19 isx53636349]# dhclient
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 192.168.3.19/16 brd 192.168.255.255 scope global dynamic enp3s0
       valid_lft 21656sec preferred_lft 21656sec
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
[root@j19 isx53636349]# 

-	pongo ip fija 

[root@j19 isx53636349]# ip a a 172.17.0.1/16 dev usb19
[root@j19 isx53636349]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 192.168.3.19/16 brd 192.168.255.255 scope global dynamic enp3s0
       valid_lft 21605sec preferred_lft 21605sec
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb19: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 44:44:44:00:00:19 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global usb19
       valid_lft forever preferred_lft forever

-	activo bit forwarding

[root@j19 isx53636349]# echo 1 > /proc/sys/net/ipv4/ip_forward


-	activo enmascaramiento de ip

[root@j19 isx53636349]# iptables -t nat -A POSTROUTING -o enp3s0 -j MASQUERADE

[root@j19 isx53636349]# ip r s
default via 192.168.0.5 dev enp3s0 
172.17.0.0/16 dev usb19  proto kernel  scope link  src 172.17.0.1 linkdown 
192.168.0.0/16 dev enp3s0  proto kernel  scope link  src 192.168.3.19 
[root@j19 isx53636349]# iptables -t nat -L
Chain PREROUTING (policy ACCEPT)
target     prot opt source               destination         

Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         

Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination         
MASQUERADE  all  --  anywhere             anywhere            
[root@j19 isx53636349]# ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=2 ttl=55 time=11.0 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=55 time=11.3 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=55 time=11.2 ms
64 bytes from 8.8.8.8: icmp_seq=6 ttl=55 time=11.1 ms
64 bytes from 8.8.8.8: icmp_seq=7 ttl=55 time=11.4 ms
64 bytes from 8.8.8.8: icmp_seq=8 ttl=55 time=11.6 ms
64 bytes from 8.8.8.8: icmp_seq=9 ttl=55 time=11.1 ms
^C
--- 8.8.8.8 ping statistics ---


			ejercici 6
			
-	informe de saltos hasta papua.go.id

[root@j19 isx53636349]# mtr -n --report papua.go.id 
Start: Thu Dec  1 09:58:45 2016
HOST: j19.informatica.escoladeltr Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 192.168.0.5                0.0%    10    0.4   0.4   0.3   0.6   0.0
  2.|-- 10.1.1.199                 0.0%    10    3.9   3.4   2.3   6.6   1.4
  3.|-- 10.10.1.4                  0.0%    10    0.6   0.6   0.4   0.8   0.0
  4.|-- 80.58.67.115               0.0%    10    3.4   2.4   1.6   3.4   0.3
  5.|-- 80.58.80.77                0.0%    10    5.5   5.6   3.6   9.9   1.8
  6.|-- 80.58.81.46                0.0%    10    3.9   4.4   3.0   5.7   0.9
  7.|-- 94.142.103.177             0.0%    10    2.7   6.6   2.6  16.1   5.5
  8.|-- 94.142.121.117             0.0%    10   34.0  31.5  30.7  34.0   0.8
  9.|-- 94.142.118.239             0.0%    10   92.3 104.2  91.8 162.9  21.8
 10.|-- 84.16.15.146               0.0%    10  162.4 171.9 161.7 226.7  21.9
 11.|-- ???                       100.0    10    0.0   0.0   0.0   0.0   0.0
 12.|-- 129.250.5.33               0.0%    10  333.0 333.4 332.8 333.7   0.0
 13.|-- 129.250.3.26               0.0%    10  172.4 173.1 171.4 176.8   1.4
 14.|-- 129.250.3.49               0.0%    10  334.4 337.9 334.0 360.9   8.5
 15.|-- 129.250.3.147              0.0%    10  347.1 347.4 346.7 348.5   0.5
 16.|-- 116.51.26.38               0.0%    10  337.8 343.4 337.4 372.3  12.3
 17.|-- ???                       100.0    10    0.0   0.0   0.0   0.0   0.0
 18.|-- 180.240.204.82             0.0%    10  350.9 350.8 350.1 351.2   0.0
 19.|-- 180.240.193.205            0.0%    10  346.0 346.2 343.2 347.9   1.4
 20.|-- 180.250.209.161            0.0%    10  468.2 437.6 433.7 468.2  10.8
 21.|-- 180.250.209.166            0.0%    10  438.9 437.0 435.3 438.9   0.9
					
					21 saltos
					
[root@j19 isx53636349]# mtr -n --report papua.go.id > /tmp/papua19.txt

		
		ejercici 7

-	en que puerto escucha el serviddor cups

[root@j19 isx53636349]# netstat -utlnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      931/rpcbind         
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1513/cupsd          
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      791/postgres        
tcp        0      0 0.0.0.0:36957           0.0.0.0:*               LISTEN      929/rpc.statd       
tcp6       0      0 :::50789                :::*                    LISTEN      929/rpc.statd       
tcp6       0      0 :::111                  :::*                    LISTEN      931/rpcbind         
tcp6       0      0 ::1:631                 :::*                    LISTEN      1513/cupsd          
tcp6       0      0 ::1:5432                :::*                    LISTEN      791/postgres        
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           663/avahi-daemon: r 
udp        0      0 0.0.0.0:46384           0.0.0.0:*                           4393/dhclient       
udp        0      0 0.0.0.0:52950           0.0.0.0:*                           929/rpc.statd       
udp        0      0 0.0.0.0:18167           0.0.0.0:*                           803/dhclient        
udp        0      0 0.0.0.0:68              0.0.0.0:*                           4393/dhclient       
udp        0      0 0.0.0.0:68              0.0.0.0:*                           803/dhclient        
udp        0      0 0.0.0.0:111             0.0.0.0:*                           931/rpcbind         
udp        0      0 127.0.0.1:323           0.0.0.0:*                           674/chronyd         
udp        0      0 0.0.0.0:47704           0.0.0.0:*                           663/avahi-daemon: r 
udp        0      0 0.0.0.0:682             0.0.0.0:*                           931/rpcbind         
udp        0      0 127.0.0.1:703           0.0.0.0:*                           929/rpc.statd       
udp6       0      0 :::5353                 :::*                                663/avahi-daemon: r 
udp6       0      0 :::38229                :::*                                929/rpc.statd       
udp6       0      0 :::5946                 :::*                                4393/dhclient       
udp6       0      0 :::111                  :::*                                931/rpcbind         
udp6       0      0 :::47219                :::*                                663/avahi-daemon: r 
udp6       0      0 :::26805                :::*                                803/dhclient        
udp6       0      0 ::1:323                 :::*                                674/chronyd         
udp6       0      0 :::682                  :::*                                931/rpcbind   
		
		el puerto que escuha el servidor cups es el 631

[root@j19 isx53636349]# netstat -utlnp | grep cups
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1513/cupsd          
tcp6       0      0 ::1:631                 :::*                    LISTEN      1513/cupsd          
[root@j19 isx53636349]# 
		

-	me conecto a meteo y filtro los resultados de conexiones que usan firefox

[root@j19 isx53636349]# netstat -p | grep firefox
tcp        0      0 j19.informatica.e:42746 104.244.42.67:https     ESTABLISHED 4742/firefox        
tcp        0      0 j19.informatica.e:53168 mad01s26-in-f10.1:https ESTABLISHED 4742/firefox        
tcp        0      0 j19.informatica.e:39898 185.29.135.190:http     ESTABLISHED 4742/firefox        
tcp        0      0 j19.informatica.e:60584 mad06s10-in-f2.1e:https ESTABLISHED 4742/firefox        
tcp        0      0 j19.informatica.e:40530 mad01s25-in-f14.1:https ESTABLISHED 4742/firefox        
tcp        0      0 j19.informatica.e:36056 mad01s25-in-f6.1e:https ESTABLISHED 4742/firefox        
tcp        0      0 j19.informatica.e:52702 mad01s25-in-f3.1e:https ESTABLISHED 4742/firefox        
tcp        0      0 j19.informatica.e:41150 wk-in-f155.1e100.:https ESTABLISHED 4742/firefox        
tcp        0      0 j19.informatica.e:44910 mad01s24-in-f238.:https ESTABLISHED 4742/firefox        
tcp        0      0 j19.informatica.e:37840 mad01s25-in-f4.1e:https ESTABLISHED 4742/firefox        
tcp        0      0 j19.informatica.e:60288 104.244.42.200:https    ESTABLISHED 4742/firefox        
unix  3      [ ]         STREAM     CONNECTED     209058956 4742/firefox         
unix  3      [ ]         STREAM     CONNECTED     209057673 4742/firefox         
unix  3      [ ]         STREAM     CONNECTED     209058957 4742/firefox         
unix  3      [ ]         STREAM     CONNECTED     209059025 4742/firefox         
unix  3      [ ]         STREAM     CONNECTED     209057661 4742/firefox         
unix  3      [ ]         STREAM     CONNECTED     209057675 4742/firefox         
unix  3      [ ]         STREAM     CONNECTED     209059954 4742/firefox         
unix  3      [ ]         STREAM     CONNECTED     209059953 4742/firefox    
