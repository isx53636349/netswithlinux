# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).


IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 | 172.16.4.0 | 172.16.4.127 | .1 a .126
174.187.55.6/23 | 174.187.54.0 | 174.187.55.255 | .54.1 a 55.254 
10.0.25.253/18 | 10.0.0.0 | 174.187.63.255 | .0.1 a .63.254
209.165.201.30/27 | 209.165.201.0 | 209.165.201.31 | .0 a .30


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.

**Amb /23 tinc 510 hosts**. Justificació:
* 32 - 23 = 9 bits per host
* 2^9 = 512 combinacions posibles
* 512 - 2 adreçes reservades = 510 hosts

De /16 a /23 hi han 7 bits per fer combinacions de subxarxes. 2^7 = 128 combinacions = 128 subxarxes posibles

La màscara CIDR /23 en format binari és: 

    11111111.11111111.11111110.00000000
    
i en format decimal es: 

    255.255.254.0 


##### 3. Donada la xarxa 10.192.0.0
a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.

* 32 - 21 = 11 bits per host
* 2^11 = 2048 combinacions posibles
* 2048 - 2 adreçes reservades = 2046 hosts

la mascara de xarxa en format decimal es:
    
```
    255.255.248.0/21
```
la mascara de xarxa en format binari seria:

```
    11111111.11111111.11111000.00000000
```



b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?

> *de /10 a /21 hi han 11 bits per fer combinacions de subxarxes que superin els 1500 hosts, per tant 2^11 = 2048 subxarxes posibles*


c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.

> *de /21 a /23 hi han 2 bits per tant 2^2 = 4 subxarxes posibles,*
*si ja sabem que a la mascara /23 poden haver 9bits per host*
*2⁹ = 512 combinacions posibles menys les 2 reservades serian 510 hosts* 

##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius

a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.

> - 32 - 20 = 12 bits per host
> - 2^12 = 4096 combinacions posibles
> - 4096 - 2 reservades = 4094 hosts posibles

> la mascara de xarxa en format decimal seria

```
    255.255.240.0/20
```
> la mascara de xarxa en format binari es:

```
    11111111.11111111.11110000.00000000
```

b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.

> - 32 - 22 = 10 bits per host
> - 2^10 = 1024 combinacions posibles
> - 1024 - 2 reservades = 1022 hosts posibles

> de /20 a /22 hi han 2 bits per fer combinacions de subxarxes per tant 2^2

c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.


d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.


##### 5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?


b) Dóna la nova MX, tant en format decimal com en binari.


c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 |
Xarxa 2 |
Xarxa 3 |
Xarxa 4 |
Xarxa 5 |
Xarxa 6 |


d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.


##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?


b) Dóna la nova MX, tant en format decimal com en binari.


c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1
Xarxa 2
Xarxa 3
Xarxa 4


d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? Dóna'n la fórmula que has utilitzat per respondre a la pregunta.

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.
