#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"

```
[root@j19 ~]# dhclient -r
Killed old client process
ip[root@j19 ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet6 fe80::ae22:bff:fec7:dd1a/64 scope link 
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
[root@j19 ~]# ip r
[root@j19 ~]# 


```

Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

```
[root@j19 ~]# ip a a 2.2.2.2/24 dev enp3s0
[root@j19 ~]# ip a a 3.3.3.3/16 dev enp3s0
[root@j19 ~]# ip a a 4.4.4.4/25 dev enp3s0
[root@j19 ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ac:22:0b:c7:dd:1a brd ff:ff:ff:ff:ff:ff
    inet 2.2.2.2/24 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 3.3.3.3/16 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet 4.4.4.4/25 scope global enp3s0
       valid_lft forever preferred_lft forever
    inet6 fe80::ae22:bff:fec7:dd1a/64 scope link 
       valid_lft forever preferred_lft forever
3: wlp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 64:66:b3:a4:4c:df brd ff:ff:ff:ff:ff:ff
4: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
[root@j19 ~]# 

```

Consulta la tabla de rutas de tu equipo

```
[root@j19 ~]# ip r
2.2.2.0/24 dev enp3s0  proto kernel  scope link  src 2.2.2.2 
3.3.0.0/16 dev enp3s0  proto kernel  scope link  src 3.3.3.3 
4.4.4.0/25 dev enp3s0  proto kernel  scope link  src 4.4.4.4 
[root@j19 ~]# 

```


Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132
> - 2.2.2.2 = contesta porque es un ping a tu misma ip
> - 2.2.2.254 = "destination unreachable" esta dentro de la red pero no contesta nadie 
> - 2.2.5.2 = "Network unreachable" no esta dentro de tu red por culpa del 5
> - 3.3.3.35 = "Destination unreachable" entra dentro de la red, pero nadie tiene esta ip
> - 3.3.200.45 = "Destination unreachable" 
> - 4.4.4.8 = "Destination unreachable"
> - 4.4.4.132 = "Network unreachable" al tener la mascara /25 el .132 no entraria en la red

#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

Conecta una segunda interfaz de red por el puerto usb

Cambiale el nombre a usb0

Modifica la dirección MAC

Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

Observa la tabla de rutas

#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

Lanzar iperf en modo servidor en cada ordenador

Comprueba con netstat en qué puerto escucha

Conectarse desde otro pc como cliente

Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

Encontrar los 3 paquetes del handshake de tcp

Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas

