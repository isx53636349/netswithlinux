# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).


IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 | 172.16.4.0 | 172.16.4.127 | .1 a .126
174.187.55.6/23 | 174.187.54.0 | 174.187.55.255 | .54.1 a 55.254 
10.0.25.253/18 | 10.0.0.0 | 174.187.63.255 | .0.1 a .63.254
209.165.201.30/27 | 209.165.201.0 | 209.165.201.31 | .0 a .30


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.

**Amb /23 tinc 510 hosts**. Justificació:
* 32 - 23 = 9 bits per host
* 2^9 = 512 combinacions posibles
* 512 - 2 adreçes reservades = 510 hosts

De /16 a /23 hi han 7 bits per fer combinacions de subxarxes. 2^7 = 128 combinacions = 128 subxarxes posibles

La màscara CIDR /23 en format binari és: 

    11111111.11111111.11111110.00000000
    
i en format decimal es: 

    255.255.254.0 


##### 3. Donada la xarxa 10.192.0.0
a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.

* 32 - 21 = 11 bits per host
* 2^11 = 2048 combinacions posibles
* 2048 - 2 adreçes reservades = 2046 hosts

la mascara de xarxa en format decimal es:
    
```
    255.255.248.0/21
```
la mascara de xarxa en format binari seria:

```
    11111111.11111111.11111000.00000000
```



b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?

> *de /10 a /21 hi han 11 bits per fer combinacions de subxarxes que superin els 1500 hosts, per tant 2^11 = 2048 subxarxes posibles*


c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.

> *de /21 a /23 hi han 2 bits per tant 2^2 = 4 subxarxes posibles,*
*si ja sabem que a la mascara /23 poden haver 9bits per host*
*2⁹ = 512 combinacions posibles menys les 2 reservades serian 510 hosts* 

##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius

a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.

> - 32 - 20 = 12 bits per host
> - 2^12 = 4096 combinacions posibles
> - 4096 - 2 reservades = 4094 hosts posibles

> la mascara de xarxa en format decimal seria

```
    255.255.240.0/20
```
> la mascara de xarxa en format binari es:

```
    11111111.11111111.11110000.00000000
```

b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.

> - 32 - 22 = 10 bits per host
> - 2^10 = 1024 combinacions posibles
> - 1024 - 2 reservades = 1022 hosts posibles

> de /20 a /22 hi han 2 bits per fer combinacions de subxarxes per tant 2^2 = 4 subxarxes. Nomes podriem fer 4 subxarxes no 5

c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.

> per que siguin 5 subxarxes tindriem que fer una mascara de /23 on podriam fer 8 subxarxes pero de 510 hosts

> calculem els bits que tenim per host 32 - 23 = 9, 2^9= 512 combinacions posibles de hosts - 2 reservades tenim un total de 510 hosts

d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.

> si la xarxa es /20 tindriem un total de 12 bits per el host (32 - 20 = 12), ens donaria 2^12 = 4096 combinacions posibles - 2 reserv = 4094 hosts

> cambian la mascara /20 a la /21 tenim 2 subxarxes de 2046 (2048 - 2) cadascuna, 

##### 5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

> necesitariem 3 bits, de la mascara /12 a la /15 tenim 2^3 = 8 subxarxes

b) Dóna la nova MX, tant en format decimal com en binari.

> - mascara nova en format decimal:

```
255.254.0.0
```
> - en binari seria :


```
11111111.11111110.00000000.00000000
```


c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 | 255.242.0.0 | 255.243.255.255 | .242.0.1 fins .243.255.254
        |11111111.11110010.00000000.00000000 | 11111111.11110011.1111111111.11111111 |
Xarxa 2 | 255.244.0.0 | 255.245.255.255 | .244.0.1 fins .245.255.254
        | 11111111.11110100.00000000.00000000 | 11111111.11110101.10111111.11111111 |
Xarxa 3 | 255.248.0.0 | 255.249.255.255 | .248.0.1 fins .249.255.254
        | 11111111.11111000.00000000.00000000 | 11111111.11111001.11111111.11111111 |
Xarxa 4 | 255.247.0.0 | 255.248.255.255 | .247.0.1 fins 248.255.254
        | 11111111.11110110.00000000.00000000 | 11111111.11110111.11111111.11111111 |
Xarxa 5 |255.250.0.0 | 255.251.255.255 | .250.0.1 fins 251.255.254
        | 11111111.11111010.00000000.00000000 | 11111111.11111011.11111111.11111111 |
Xarxa 6 | 255.252.0.0 | 255.253.255.255 | .252.0.1 fins 253.255.254
        | 11111111.11111100.00000000.00000000 | 11111111.11111101.11111111.111111111 |


d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

> 3 bits, 2 ^ 3 = 8 subxarxes

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

> - 32 - 15 = 17 bits
> - 2^17 = 131072 hosts
> - 131072 - 2 reservats = 131070 hosts pot tenir cada subxarxa


##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 4 subxarxes?

> 2 bits

b) Dóna la nova MX, tant en format decimal com en binari.

> format decimal

```
255.255.255.192
```
> format binari

```
11111111.11111111.11111111.11000000
```


c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 | 255.255.255.0 |255.255.255.63 | .1 fins .62
        |11111111.11111111.11111111.00000000 | 11111111.11111111.11111111.00111111 |
Xarxa 2 | 255.255.255.64| 255.255.255.127 | .65 fins .126
        | 11111111.11111111.11111111.01000000 | 11111111.11111111.11111111.01111111 |
Xarxa 3 | 255.255.255.128 | 255.255.255.191 | .129 fins .190
        | 11111111.11111111.11111111.10000000 | 11111111.11111111.11111111.10111111 |
Xarxa 4 | 255.255.255.192 | 255.255.255.255 | .193 fins .254
        | 11111111.11111111.11111111.11000000 | 11111111.11111111.11111111.1111111 | 


d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? Dóna'n la fórmula que has utilitzat per respondre a la pregunta.

> no podriem perque nomes hem agafat 2 bits mes, i aixó ens dona 2^2 = 4 subxarxes o sigui un maxim de 4 subxarxes podem fer

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

> la nova mascara seria /26 
> - 32 - 26 = 6 bits
> - 2^6 = 64 hosts
> - 64 - 2 reservats = 62 hosts podriem tenir a cada subxarxa.
